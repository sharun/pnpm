# Maintainer: Daniel M. Capella <polyzen@archlinux.org>
# Contributor: Severen Redwood <me@severen.dev>
# Contributor: Tomasz Jakub Rup <tomasz.rup@gmail.com>

pkgname=pnpm
pkgver=8.15.2
pkgrel=2
pkgdesc='Fast, disk space efficient package manager'
arch=('any')
url=https://pnpm.io
license=('MIT')
depends=('node-gyp')
makedepends=('git' 'pnpm')
source=("git+https://github.com/$pkgname/$pkgname.git#tag=v$pkgver?signed")
b2sums=('7abefa614be20bf90e1bf7cf47ee0d2700c5085931a91412fb477d9ea5cc75bf5e10a75a047afa4bef5fd0db5e99258c995a9f3f465d4b04ab6991afc47fcbc2')
validpgpkeys=('7B74D1299568B586BA9962B5649E4D4AF74E7DEC') # Zoltan Kochan <z@kochan.io>

build() {
  cd $pkgname/$pkgname
  git cherry-pick --no-commit 7f0aae82db3628581bc704a394ef025ab258ab28

  pnpm install --frozen-lockfile
  pnpm run compile
}

package() {
  local mod_dir=/usr/lib/node_modules/$pkgname
  install -d "$pkgdir"/{usr/bin,$mod_dir/dist}
  ln -s $mod_dir/bin/$pkgname.cjs "$pkgdir"/usr/bin/$pkgname
  ln -s $mod_dir/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx

  cd $pkgname/$pkgname
  cp -r bin package.json "$pkgdir"/$mod_dir
  install -Dt "$pkgdir"/usr/share/licenses/$pkgname LICENSE
  cd dist
  cp -r $pkgname.cjs pnpmrc scripts worker.js "$pkgdir"/$mod_dir/dist
}
